from django.urls import path
from photos.views import PhotoListCreateView

urlpatterns = [
    path("photos/", PhotoListCreateView.as_view(), name="photos"),
]
