from typing import Type

from django.db import models
from photos.models import Photo
from photos.serializers.photo import PhotoCreateSerializer, PhotoListSerializer
from rest_framework import generics, status
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response


class PhotoListCreateView(generics.ListCreateAPIView):
    queryset = Photo.objects.all()
    parser_classes = (MultiPartParser, FormParser, JSONParser)
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(
        self,
    ) -> Type[PhotoCreateSerializer] | Type[PhotoListSerializer]:
        if self.request.method == "POST":
            return PhotoCreateSerializer
        return PhotoListSerializer

    def get_tier_list(self):
        user_tier = self.request.user.tier
        fields = tuple(
            field.name
            for field in user_tier._meta.fields
            if isinstance(field, models.BooleanField) and getattr(user_tier, field.name)
        )
        return fields

    def list(self, request: Request, *args, **kwargs) -> Response:
        queryset = Photo.objects.filter(owner=self.request.user).order_by(
            "-upload_date"
        )
        fields = self.get_tier_list()
        serializer = PhotoListSerializer(queryset, many=True, fields=fields)
        return Response(serializer.data)

    def create(self, request: Request, *args, **kwargs) -> Response:
        serializer = PhotoCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        image = serializer.save(owner=request.user)
        response_data = PhotoListSerializer(
            instance=image, fields=self.get_tier_list()
        ).data
        return Response(response_data, status=status.HTTP_201_CREATED)
