from __future__ import annotations

import os
from io import BytesIO

from django.core.files import File
from django.db import models
from PIL import Image
from users.models import User


class Photo(models.Model):
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        help_text="Owner of the image",
    )
    upload_date = models.DateTimeField(auto_now_add=True)
    source_image = models.ImageField(
        upload_to="source",
        blank=False,
        help_text="URL to locally stored file",
    )
    thumbnail_200px = models.ImageField(
        upload_to="200px",
        null=True,
        blank=True,
        help_text="URL to locally stored 200px thumbnail",
    )
    thumbnail_400px = models.ImageField(
        upload_to="400px",
        null=True,
        blank=True,
        help_text="URL to locally stored 400px thumbnail",
    )

    def save(self, *args, **kwargs) -> None:
        """Call create_thumbnail methods before save operation"""
        if self.owner.tier.thumbnail_200px:
            self.create_thumbnail(200, self.thumbnail_200px)
        if self.owner.tier.thumbnail_400px:
            self.create_thumbnail(400, self.thumbnail_400px)

        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return f"{self.source_image.name}"

    def create_thumbnail(self, thumbnail_height: int, field: models.ImageField) -> None:
        """Create thumbnail based on Photo.source_image and passed arguments"""
        with Image.open(self.source_image) as img:
            width: int = round((thumbnail_height / img.height) * img.width)
            thumb: Image = img.resize((width, thumbnail_height))
            thumbnail_name, thumbnail_extension = os.path.splitext(
                self.source_image.name
            )
            thumbnail_name: str = (
                f"{thumbnail_name}_{thumbnail_height}px{thumbnail_extension}"
            )

            if thumbnail_extension.lower() in [".jpg", ".jpeg"]:
                format_specifier: str = "JPEG"
            else:
                format_specifier: str = "PNG"

            temp_storage: BytesIO = BytesIO()
            thumb.save(temp_storage, format_specifier)

        field.save(f"{thumbnail_name}", File(temp_storage), save=False)
        temp_storage.close()
