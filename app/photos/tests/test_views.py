import io
import shutil
import tempfile

from django.core.files.uploadedfile import SimpleUploadedFile
from django.shortcuts import reverse
from factories.photo import PhotoFactory
from factories.user import UserFactory
from photos.models import Photo
from PIL import Image
from rest_framework import status
from rest_framework.test import APITestCase, override_settings

MEDIA_ROOT = tempfile.mkdtemp()


def generate_image():
    image = Image.new("RGB", size=(300, 300), color="black")
    with io.BytesIO() as output:
        image.save(output, format="JPEG")
        image_file = SimpleUploadedFile(
            "image.jpg", output.getvalue(), content_type="image/jpeg"
        )
    return image_file


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestPhotosView(APITestCase):
    def test_photos_view_should_return_403_status_for_not_logged_user(self):
        response = self.client.get(reverse("photos"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_photos_view_should_return_only_photos_owned_by_request_user(self):
        owner = UserFactory()
        another_user = UserFactory()
        PhotoFactory(owner=owner)
        PhotoFactory(owner=another_user)
        self.client.force_login(owner)
        response = self.client.get(reverse("photos"))
        self.assertEqual(len(response.data), 1)

    def test_photos_view_valid_photo_should_result_in_instance_creation(self):
        image_file = generate_image()
        self.client.force_login(UserFactory())
        self.client.post(reverse("photos"), data={"source_image": image_file})
        self.assertEqual(Photo.objects.count(), 1)

    def test_photos_view_response_fields_should_match_user_tier(self):
        user = UserFactory()
        image_file = generate_image()
        self.client.force_login(user)
        response = self.client.post(
            reverse("photos"), data={"source_image": image_file}
        )
        expected = "thumbnail_200px"
        not_expected = "source_image"
        self.assertIn(expected, response.data)
        self.assertNotIn(not_expected, response.data)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()
