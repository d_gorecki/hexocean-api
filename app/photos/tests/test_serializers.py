import io
import shutil
import tempfile

from django.core.files.uploadedfile import SimpleUploadedFile
from factories.photo import PhotoFactory
from photos.serializers.photo import PhotoCreateSerializer, PhotoListSerializer
from PIL import Image
from rest_framework.exceptions import ValidationError
from rest_framework.test import APITestCase, override_settings

MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestPhotoListSerializer(APITestCase):
    def setUp(self) -> None:
        self.photo = PhotoFactory()

    def test_serializer_contains_expected_fields(self):
        cases = [
            ("thumbnail_200px",),
            ("thumbnail_200px", "source_image"),
            ("thumbnail_200px", "source_image", "thumbnail_400px"),
        ]

        for case in cases:
            serializer = PhotoListSerializer(instance=self.photo, fields=case)
            data = serializer.data
            expected_fields = set(case)
            self.assertCountEqual(set(data.keys()), expected_fields)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class PhotoCreateSerializerTest(APITestCase):
    def test_photo_create_serializer_should_raise_exception_for_invalid_image_format(
        self,
    ):
        image = Image.new("RGB", size=(300, 300), color="black")
        with io.BytesIO() as output:
            image.save(output, format="JPEG")
            image_file = SimpleUploadedFile(
                "wrong_extension.notjpg", output.getvalue(), content_type="image/jpeg"
            )

        with self.assertRaises(ValidationError):
            serializer = PhotoCreateSerializer(data={"source_image": image_file})
            serializer.is_valid(raise_exception=True)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()
