import shutil
import tempfile

from django.test import TestCase, override_settings
from factories.photo import PhotoFactory
from factories.tier import TierFactory
from factories.user import UserFactory
from photos.models import Photo

MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestPhoto(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.basic, cls.premium, cls.enterprise = TierFactory.create_base_tiers()

    def test_save_method_should_generate_200px_thumbnail_for_tier_with_200px_option(
        self,
    ):
        PhotoFactory()
        image = Photo.objects.first()
        self.assertNotEqual(image.thumbnail_200px, "")

    def test_save_method_should_generate_400px_thumbnail_for_tier_with_400px_option(
        self,
    ):
        PhotoFactory(owner=UserFactory(tier=self.premium))
        image = Photo.objects.first()
        self.assertNotEqual(image.thumbnail_400px, "")

    def test_generated_thumbnail_should_have_proper_height(self):
        PhotoFactory(owner=UserFactory(tier=self.enterprise))
        image = Photo.objects.first()
        self.assertEqual(image.thumbnail_200px.height, 200)
        self.assertEqual(image.thumbnail_400px.height, 400)

    def test_str_method_should_return_proper_object_representation(self):
        img = PhotoFactory()
        expected = f"{img.source_image.name}"
        self.assertEqual(expected, str(img))

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()
