from django.core.validators import FileExtensionValidator
from photos.models import Photo
from rest_framework import serializers


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """Dynamically set serializer fields based on passed arguments"""

    def __init__(self, *args, **kwargs):
        fields: dict = kwargs.pop("fields", None)
        super().__init__(*args, **kwargs)

        if fields:
            allowed: set = set(fields)
            existing: set = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class PhotoListSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = Photo
        fields = (
            "owner",
            "upload_date",
            "source_image",
            "thumbnail_200px",
            "thumbnail_400px",
        )
        read_only_fields = fields


class PhotoCreateSerializer(serializers.ModelSerializer):
    source_image = serializers.ImageField(
        validators=[FileExtensionValidator(allowed_extensions=["png", "jpg"])]
    )

    class Meta:
        model = Photo
        fields = ("source_image",)
