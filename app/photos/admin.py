from django.contrib import admin
from photos.models import Photo


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    list_display = (
        "owner",
        "upload_date",
        "source_image",
        "thumbnail_200px",
        "thumbnail_400px",
    )

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "owner",
                    "source_image",
                )
            },
        ),
    )

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "owner",
                    "source_image",
                ),
            },
        ),
    )
