from django.test import TestCase
from tiers.models import Tier
from users.models import User


class TestUser(TestCase):
    def test_str_method_should_return_proper_object_representation(self):
        expected = "TestUser"
        tier = Tier.objects.create(name="test")
        user = User.objects.create(username=expected, tier=tier)
        self.assertEqual(expected, str(user))
