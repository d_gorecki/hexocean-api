from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from tiers.models import Tier
from users.models import User


class TestLoginLogoutView(APITestCase):
    def setUp(self) -> None:
        self.correct_username = "test"
        self.incorrect_username = "wrongusername"
        self.correct_passwd = "zaq1@WSX"
        self.incorrect_passwd = "@WSXzaq`"
        tier = Tier.objects.create(name="test")
        self.user = User.objects.create_user(
            username=self.correct_username, password=self.correct_passwd, tier=tier
        )

    def test_login_view_should_return_400_response_for_incorrect_username(self):
        user_credentials = {
            "username": self.incorrect_username,
            "password": self.correct_passwd,
        }
        response = self.client.post(reverse("auth-login"), user_credentials)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_view_should_return_400_response_for_incorrect_password(self):
        user_credentials = {
            "username": self.correct_username,
            "password": self.incorrect_passwd,
        }
        response = self.client.post(reverse("auth-login"), user_credentials)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_view_should_return_200_response_for_successful_login(self):
        user_credentials = {
            "username": self.correct_username,
            "password": self.correct_passwd,
        }
        response = self.client.post(reverse("auth-login"), user_credentials)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"detail": "Successfully logged in"})

    def test_logout_view_should_return_200_response_for_logging_out_authenticated_user(
        self,
    ):
        self.client.login(username=self.correct_username, password=self.correct_passwd)
        response = self.client.get(reverse("auth-logout"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"detail": "Successfully logged out"})

    def test_logout_view_should_return_400_response_if_called_by_not_logged_user(self):
        response = self.client.get(reverse("auth-logout"))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {"detail": "You are not logged in"})
