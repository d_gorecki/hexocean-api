from django.test import TestCase
from rest_framework.serializers import ValidationError
from users.serializers import LoginSerializer


class TestLoginSerializer(TestCase):
    def test_login_serializer_validate_method_should_raise_validation_error_for_non_existing_user(
        self,
    ):
        serializer = LoginSerializer()
        attrs = {"username": "not_exist", "password": "not-exist"}
        with self.assertRaises(ValidationError):
            serializer.validate(attrs)
