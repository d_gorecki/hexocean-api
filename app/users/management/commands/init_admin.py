from django.core.management.base import BaseCommand
from tiers.models import Tier
from users.models import User


class Command(BaseCommand):
    """Create default admin user"""

    def handle(self, *args, **options) -> None:
        """Entrypoint for command."""
        if not User.objects.filter(is_superuser=True):
            User.objects.create_superuser(
                username="admin", password="admin", tier=Tier.objects.get(id=3)
            )
