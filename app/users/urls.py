from rest_framework import routers
from users.views import UserAuthViewSet

router = routers.DefaultRouter()
router.register(r"", viewset=UserAuthViewSet, basename="auth")

urlpatterns = router.urls
