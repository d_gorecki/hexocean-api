from django.contrib.auth.models import AbstractUser
from django.db import models
from tiers.models import Tier


class User(AbstractUser):
    tier = models.ForeignKey(Tier, on_delete=models.PROTECT, help_text="Tier type")

    def __str__(self) -> str:
        return str(self.username)
