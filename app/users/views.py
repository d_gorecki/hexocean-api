from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.request import Request
from rest_framework.response import Response
from users.models import User
from users.serializers import LoginSerializer


class UserAuthViewSet(viewsets.GenericViewSet):
    """
    User authentication viewset consisting of following views:
     - login,
     - logout,
    """

    permission_classes = (AllowAny,)

    @action(detail=False, methods=["get"])
    def whoami(self, request):
        return Response({"user": request.user.username})

    @action(detail=False, methods=["post"], serializer_class=LoginSerializer)
    def login(self, request: Request) -> Response:
        serializer: LoginSerializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user: User = serializer.validated_data["user"]
        django_login(request, user)
        return Response({"detail": "Successfully logged in"}, status=status.HTTP_200_OK)

    @action(detail=False, methods=["get"])
    def logout(self, request: Request) -> Response:
        if not request.user.is_authenticated:
            return Response(
                {"detail": "You are not logged in"}, status=status.HTTP_400_BAD_REQUEST
            )

        django_logout(request)
        return Response(
            {"detail": "Successfully logged out"}, status=status.HTTP_200_OK
        )
