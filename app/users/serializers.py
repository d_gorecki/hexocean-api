from django.contrib.auth import authenticate
from rest_framework import serializers
from users.models import User


class LoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField(write_only=True, required=True)
    password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ("username", "password")

    def validate(self, attrs: dict) -> dict:
        """Perform user validation"""
        user: User = authenticate(
            username=attrs.get("username"), password=attrs.get("password")
        )

        if not user:
            raise serializers.ValidationError({"detail": "Wrong email or password."})

        attrs["user"]: User = user

        return attrs
