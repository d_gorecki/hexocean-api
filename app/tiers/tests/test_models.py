from django.test import TestCase
from tiers.models import Tier


class TestTier(TestCase):
    def test_str_method_should_return_proper_object_representation(self):
        expected = "TestTier"
        tier = Tier.objects.create(name=expected)
        self.assertEqual(expected, str(tier))
