from django.db import models


class Tier(models.Model):
    name = models.CharField(
        max_length=20, null=False, blank=False, help_text="Tier name"
    )
    thumbnail_200px = models.BooleanField(
        default=True, help_text="Link for 200px thumbnail available"
    )
    thumbnail_400px = models.BooleanField(
        default=False, help_text="Link for 400px thumbnail available"
    )
    source_image = models.BooleanField(
        default=False, help_text="Link for source image thumbnail available"
    )
    fetch_link = models.BooleanField(
        default=False, help_text="Permission to fetch link to source image"
    )

    def __str__(self) -> str:
        return str(self.name)
