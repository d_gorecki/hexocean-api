from django.core.management.base import BaseCommand
from tiers.models import Tier


class Command(BaseCommand):
    """Create initial Tier models"""

    def handle(self, *args, **options) -> None:
        """Entrypoint for command."""
        if not Tier.objects.first():
            Tier.objects.create(name="Basic")
            Tier.objects.create(name="Premium", thumbnail_400px=True, source_image=True)
            Tier.objects.create(
                name="Enterprise",
                thumbnail_400px=True,
                source_image=True,
                fetch_link=True,
            )
