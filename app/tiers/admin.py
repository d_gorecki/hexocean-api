from django.contrib import admin
from tiers.models import Tier


@admin.register(Tier)
class TierAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    search_help_text = "Tier name"
    ordering = ("name",)
    list_display = (
        "name",
        "thumbnail_200px",
        "thumbnail_400px",
        "source_image",
        "fetch_link",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "thumbnail_200px",
                    "thumbnail_400px",
                    "source_image",
                    "fetch_link",
                )
            },
        ),
    )

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "name",
                    "thumbnail_200px",
                    "thumbnail_400px",
                    "source_image",
                    "fetch_link",
                ),
            },
        ),
    )
