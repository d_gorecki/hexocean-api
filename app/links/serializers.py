from django.db.models import QuerySet
from links.models import ExpiringLink
from photos.models import Photo
from rest_framework import serializers


class ExpiringLinksListSerializer(serializers.ModelSerializer):
    image = serializers.StringRelatedField()
    expiration_datetime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = ExpiringLink
        fields = ("image", "link", "expiration_datetime")
        read_only_fields = ("link", "expiration_datetime")


class UserImagePrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self) -> QuerySet:
        """Restrict available Photo instances to ones owned by request.user"""
        user = self.context["request"].user
        queryset = Photo.objects.filter(owner=user)

        return queryset


class ExpiringLinksCreateSerializer(serializers.ModelSerializer):
    expiration_time = serializers.IntegerField(min_value=300, max_value=30_000)
    image = UserImagePrimaryKeyRelatedField()

    class Meta:
        model = ExpiringLink
        fields = ("id", "image", "expiration_time")
        read_only_fields = ("id",)
