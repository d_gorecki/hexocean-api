from datetime import timedelta
from typing import Type

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import QuerySet
from django.shortcuts import redirect
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from links.models import ExpiringLink
from links.permissions import IsTierSuperuserPermission
from links.serializers import (ExpiringLinksCreateSerializer,
                               ExpiringLinksListSerializer)
from rest_framework import generics, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView


class CreateListExpiringLinks(generics.ListCreateAPIView):
    """View to list user's links and create new links to images"""

    permission_classes = (IsAuthenticated, IsTierSuperuserPermission)

    def get_queryset(self) -> QuerySet:
        return ExpiringLink.objects.select_related("image").filter(
            image__owner=self.request.user
        )

    def get_serializer_class(
        self,
    ) -> Type[ExpiringLinksCreateSerializer] | Type[ExpiringLinksListSerializer]:
        if self.request.method == "POST":
            return ExpiringLinksCreateSerializer
        return ExpiringLinksListSerializer

    def create(self, request: Request, *args, **kwargs) -> Response:
        serializer: ExpiringLinksCreateSerializer = self.get_serializer(
            data=request.data
        )
        serializer.is_valid(raise_exception=True)
        expiration_seconds: int = serializer.validated_data.pop("expiration_time")
        link = serializer.save()
        encoded_id: str = urlsafe_base64_encode(force_bytes(link.id))
        full_url: str = request.build_absolute_uri(
            reverse("links-retrieve", kwargs={"encoded_id": encoded_id})
        )

        link.link = full_url
        link.expiration_datetime = timezone.now() + timedelta(
            seconds=expiration_seconds
        )
        link.save(update_fields=["link", "expiration_datetime"])

        return Response({"detail": full_url}, template_name="rest_framework/api.html")


class RetrieveExpiringLinks(APIView):
    """View to process expiring link to image"""

    permission_classes = (AllowAny,)

    def get(self, request, encoded_id: str) -> redirect:
        try:
            decoded_id: str = urlsafe_base64_decode(encoded_id).decode()
            link: ExpiringLink = ExpiringLink.objects.get(id=decoded_id)
        except (ValueError, ObjectDoesNotExist):
            return Response(
                {"detail": "Link is not valid"}, status=status.HTTP_400_BAD_REQUEST
            )

        if timezone.now() > link.expiration_datetime:
            return Response(
                {"error": "Link has expired"}, status=status.HTTP_401_UNAUTHORIZED
            )

        redirect_url: str = (
            f"{request.scheme}://{request.get_host()}{settings.MEDIA_URL}{link.image}"
        )
        return redirect(redirect_url)
