import shutil
import tempfile

from django.test import TestCase, override_settings
from factories.link import LinkFactory

MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestTier(TestCase):
    def setUp(self) -> None:
        self.expected = "https://link.com"
        self.link = LinkFactory(link=self.expected)

    def test_str_method_should_return_proper_object_representation(self):
        self.assertEqual(self.expected, str(self.link))

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()
