import shutil
import tempfile
from datetime import timedelta

from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from factories.link import LinkFactory
from factories.photo import PhotoFactory
from factories.tier import TierFactory
from factories.user import UserFactory
from links.models import ExpiringLink
from rest_framework import status
from rest_framework.test import APITestCase, override_settings

MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestCreateListExpiringLinksView(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.basic, cls.premium, cls.enterprise = TierFactory.create_base_tiers()

    def setUp(self) -> None:
        pass

    def test_expiringlinks_list_view_should_return_403_response_for_not_logged_user(
        self,
    ):
        response = self.client.get(reverse("links"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_expiringlinks_list_view_should_return_403_response_for_user_with_no_tier_permission(
        self,
    ):
        no_perms_user = UserFactory(tier=self.basic)
        self.client.force_login(no_perms_user)
        response = self.client.get(reverse("links"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_expiringlinks_list_view_should_be_accessible_for_logged_user_with_proper_tier_permissions(
        self,
    ):
        user = UserFactory(tier=self.enterprise)
        self.client.force_login(user)
        response = self.client.get(reverse("links"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_expiringlinks_list_view_should_return_only_logged_user_links(self):
        owner = UserFactory(tier=self.enterprise)
        another_user = UserFactory(tier=self.enterprise)
        LinkFactory(image=PhotoFactory(owner=owner))
        LinkFactory(image=PhotoFactory(owner=another_user))
        self.client.force_login(owner)
        response = self.client.get(reverse("links"))
        self.assertEqual(len(response.data), 1)

    def test_expiringlinks_create_view_should_return_valid_link(self):
        user = UserFactory(tier=self.enterprise)
        self.client.force_login(user)
        image = PhotoFactory(owner=user)
        response = self.client.post(
            reverse("links"), data={"image": image.id, "expiration_time": 500}
        )
        link = ExpiringLink.objects.first()
        encoded_id = urlsafe_base64_encode(force_bytes(link.id))
        expected = f"http://testserver/api/t/{encoded_id}/"

        self.assertEqual(response.data["detail"], expected)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestRetrieveExpiringLinksView(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.basic, cls.premium, cls.enterprise = TierFactory.create_base_tiers()

    def setUp(self) -> None:
        user = UserFactory(tier=self.enterprise)
        self.client.force_login(user)
        image = PhotoFactory(owner=user)
        response = self.client.post(
            reverse("links"), data={"image": image.id, "expiration_time": 500}
        )
        self.link = ExpiringLink.objects.first()
        self.encoded_id = urlsafe_base64_encode(force_bytes(self.link.id))

    def test_expiringlinks_retrive_view_should_return_400_response_for_invalid_link(
        self,
    ):
        response = self.client.get(
            reverse("links-retrieve", kwargs={"encoded_id": "abcd"})
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_expiringlinks_retrieve_view_should_return_401_response_for_expired_link(
        self,
    ):
        self.link.expiration_datetime = timezone.now() - timedelta(days=1)
        self.link.save()
        response = self.client.get(
            reverse("links-retrieve", kwargs={"encoded_id": self.encoded_id})
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_expiringlinks_retrieve_view_should_return_redirect_for_valid_link(self):
        response = self.client.get(
            reverse("links-retrieve", kwargs={"encoded_id": self.encoded_id}),
            follow=False,
        )
        expected_url = f"http://testserver{settings.MEDIA_URL}{self.link.image}"
        self.assertRedirects(response, expected_url, fetch_redirect_response=False)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()
