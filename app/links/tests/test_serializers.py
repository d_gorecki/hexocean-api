import shutil
import tempfile

from factories.link import LinkFactory
from links.serializers import ExpiringLinksListSerializer
from rest_framework.test import APITestCase, override_settings

MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class TestExpiringLinksListSerializer(APITestCase):
    def setUp(self) -> None:
        self.link = LinkFactory()
        self.serializer = ExpiringLinksListSerializer(instance=self.link)

    def test_expiringlinks_list_serializer_contains_expected_fields(self):
        data = self.serializer.data
        expected_fields = {
            "image",
            "link",
            "expiration_datetime",
        }
        self.assertCountEqual(set(data.keys()), expected_fields)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()
