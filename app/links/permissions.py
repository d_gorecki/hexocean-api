from rest_framework import permissions


class IsTierSuperuserPermission(permissions.BasePermission):
    """Designates if user has access to particular view"""

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.user.tier.fetch_link:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        """Designates if user has access to particular instance"""
        if request.user.is_superuser:
            return True
        if obj.image.owner == request.user:
            return True
        return False
