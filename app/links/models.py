import uuid

from django.db import models
from photos.models import Photo


class ExpiringLink(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    image = models.ForeignKey(Photo, on_delete=models.CASCADE, help_text="Image")
    link = models.URLField(blank=True, null=True, help_text="Expiring link to image")
    created = models.DateTimeField(
        auto_now_add=True, help_text="Date and time of link creation"
    )
    expiration_datetime = models.DateTimeField(
        blank=True, null=True, help_text="Link valid until"
    )

    def __str__(self) -> str:
        return f"{self.link}"
