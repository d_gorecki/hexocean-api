from django.urls import path
from links.views import CreateListExpiringLinks, RetrieveExpiringLinks

urlpatterns = [
    path("links/", CreateListExpiringLinks.as_view(), name="links"),
    path("t/<str:encoded_id>/", RetrieveExpiringLinks.as_view(), name="links-retrieve"),
]
