"""from django.contrib import admin
from links.models import ExpiringLink

admin.site.register(ExpiringLink)
"""


from django.contrib import admin
from links.models import ExpiringLink


@admin.register(ExpiringLink)
class ExpiringLink(admin.ModelAdmin):
    def get_owner(self, obj):
        return obj.image.owner

    get_owner.short_description = "Owner"

    search_fields = ("photo",)
    ordering = ("created",)
    list_display = (
        "get_owner",
        "image",
        "link",
        "created",
        "expiration_datetime",
    )

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "image",
                    "link",
                    "expiration_datetime",
                )
            },
        ),
    )

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "image",
                    "link",
                    "expiration_datetime",
                ),
            },
        ),
    )
