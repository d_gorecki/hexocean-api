import factory
from factories.tier import TierFactory
from users.models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: f"user{n}")
    password = factory.PostGenerationMethodCall("set_password", "zaq1@WSX")
    tier = factory.SubFactory(TierFactory)
