import factory
from django.utils import timezone
from factories.user import UserFactory
from photos.models import Photo


class PhotoFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Photo

    owner = factory.SubFactory(UserFactory)
    upload_date = factory.LazyFunction(timezone.now)
    source_image = factory.django.ImageField(width=300, height=300, color="black")
    thumbnail_200px = None
    thumbnail_400px = None
