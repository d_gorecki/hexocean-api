from datetime import timedelta

import factory
from django.utils import timezone
from factories.photo import PhotoFactory
from links.models import ExpiringLink


class LinkFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExpiringLink

    id = factory.Faker("uuid4")
    image = factory.SubFactory(PhotoFactory)
    link = factory.Faker("uri")
    created = timezone.now()
    expiration_datetime = timezone.now() + timedelta(seconds=500)
