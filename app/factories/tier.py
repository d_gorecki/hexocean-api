import factory
from tiers.models import Tier


class TierFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tier

    name = factory.Sequence(lambda n: "tier{n}")
    thumbnail_200px = True
    thumbnail_400px = False
    source_image = False
    fetch_link = False

    @classmethod
    def create_base_tiers(cls):
        return (
            cls(name="Basic"),
            cls(name="Premium", thumbnail_400px=True, source_image=True),
            cls(
                name="Premium", thumbnail_400px=True, source_image=True, fetch_link=True
            ),
        )
