# hexocean_api

## Table of contents
* [General info](#general-info)
* [Environment](#environment)
* [Setup](#setup)


## General info
hexocean_api is django/DRF based web API that enables users to store images and to generate expiring links to uploaded images.
This document describes how to deploy the application in a local environment using docker and docker-compose.

## Environment
In order to build the application you need to define environment variables in the .env file.  \
Below you will find a sample .env file.
```
DB_HOST=db
DB_NAME=dbname
DB_USER=rootuser
DB_PASS=changeme
DJANGO_SECRET_KEY=changeme
```

**Important! .env file must be located in the same directory as Dockerfile and docker-compose.yml (/deployment/local).**

## Setup
1. Clone the repository: \
`$ git clone https://gitlab.com/d_gorecki/hexocean-api.git`
2. Create and set .env file as described in [Environment](#environment) section
2. Run following command in hexocean-api/deployment/local directory: \
`$ docker-compose up --build`

Proper setup should result in an endpoint(API documentation) available at **`localhost:8000/api/docs/`**  \
Initial admin account credentials `login: admin`, `password: admin`
