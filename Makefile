default: format

format:
	echo "Formatting app directory"
	black app
	isort app --profile black
	echo "Formatted successfully"

tests:
	echo "Running tests"
	python app/manage.py test

lint:
	echo "Linting app directory"
	flake8 app/
	echo "Linted successfully"

run:
	echo "Running app"
	python app/manage.py runserver
	echo "Running successfully"

checkmigrations:
	python app/manage.py makemigrations --check --no-input --dry-run

makemigrations:
	python app/manage.py makemigrations
